
const env = process.env.NODE_ENV === "development" ? "development" : "production"
console.log("🚀 ~ file: ormconfig.js ~ line 3 ~ isDevelopment", env)

const prodDbConnection = {
  host: process.env.DB_HOST,
  username: process.env.DB_USERNAME,
  database: process.env.DATABASE,
  password: process.env.DB_PASSWORD,
}

const devDbConnection = {
  host: "postgres",
  username: "postgres",
  password: "password",
  database: "art-gallery",
}

const dbConnection = () => {
  return env === 'development'
  ? devDbConnection
  : prodDbConnection
}

const config = {
  ...dbConnection(), 
   "type": "postgres",
   "port": 5432,
   "synchronize": false,
   "logging": true,
   "entities": [
      "src/entity/**/*.ts"
   ],
   "migrations": [
      "src/migration/**/*.ts"
   ],
   "subscribers": [
      "src/subscriber/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   }
}

console.log('dbConnection', dbConnection())
module.exports = config
